import { IsPositive, IsNotEmpty } from 'class-validator';
class CreatedOrderItemDto {
  @IsNotEmpty()
  productId: number;
  @IsPositive()
  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  customerID: number;
  @IsNotEmpty()
  orderItem: CreatedOrderItemDto[];
}
